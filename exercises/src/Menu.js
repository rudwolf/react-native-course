import React from 'react';
import { createDrawerNavigator } from 'react-navigation';
import Simple from './components/Simple';
import OddOrEven from './components/OddOrEven';
import Invert, { Lottery } from './components/Multi';
import Counter from './components/Counter';
import Platforms from './components/Platforms';
import ValidateProps from './components/ValidateProps';
import EventManager from './components/EventManager';
import Grampa from './components/DirectCommunication';

export default createDrawerNavigator({
    Grampa: {
        screen: () => <Grampa name='Jonas' lastname='Doe' />
    },
    EventManager: {
        screen: EventManager
    },
    ValidateProps: {
        screen: () => <ValidateProps year={19} />
    },
    Platforms: {
        screen: Platforms
    },
    Counter: {
        screen: () => <Counter startAt={10} />
    },
    Lottery: {
        screen: () => <Lottery numbers={6} />,
        navigationOptions: { title: 'Lottery Numbers'}
    },
    Invert: {
        screen: () => <Invert text="React Native!" />
    },
    OddOrEven: {
        screen: () => <OddOrEven number={30} />,
        navigationOptions: { title: 'Odd or Even?'}
    },
    Simple: {
        screen: () => <Simple text="Flex it!" />
    },
}, { drawerWidth: 300});