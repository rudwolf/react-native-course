/*jshint esversion: 6 */
import React from 'react';
import { Text } from 'react-native';
import Standard from '../style/Standard';

export default props =>
    <Text style={[Standard.ex]}>Arrow 1: {props.text}</Text>;

