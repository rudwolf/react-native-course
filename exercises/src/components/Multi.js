import React from 'react';
import { Text } from 'react-native';
import Standard from '../style/Standard';

const Invert = props => {
    const inv = props.text.split('').reverse().join('');
    return <Text style={Standard.ex}>{inv}</Text>
}

const Lottery = props => {
    const [min, max] = [1, 60];
    const numbers = Array(props.numbers || 6).fill(0);

    for (let index = 0; index < numbers.length; index++) {
        let newNumber = 0;
        while (numbers.includes(newNumber)) {
            newNumber = Math.floor(Math.random() * (max - min + 1)) + min;
        }
        numbers[index] = newNumber;
    }
    numbers.sort((a, b) => a -b);
    return <Text style={Standard.ex}>{numbers.join(', ')}</Text>;
}

export default Invert;
export {Invert, Lottery};