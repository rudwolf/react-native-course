import React from 'react';
import {View, Text} from 'react-native';

const font = { style: { fontSize: 30} };

function ChildWalker(props) {
    return React.Children.map(props.children,
        c => React.cloneElement(c, { ...props, ...c.props}))
}

export const Child = props =>
    <View>
        <Text {...font}>Child: {props.name} {props.lastname}</Text>
    </View>

export const Father = props =>
    <View>
        <Text {...font}>Father: {props.name} {props.lastname}</Text>
        {/* {props.children} */}
        {ChildWalker(props)}
    </View>

export const Grampa = props =>
    <View>
        <Text {...font}>Grampa: {props.name} {props.lastname}</Text>
        <Father name="John" lastname={props.lastname}>
            <Child name="Mali" />
            <Child name="Mary" />
        </Father>
        <Father {...props} name="Peter">
            <Child name="Ronald" />
            <Child name="Rick" />
            <Child name="Rosana" />
        </Father>
    </View>

export default Grampa;