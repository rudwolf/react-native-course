import React, { Component } from 'react';
import { View, Text, TouchableHighlight } from 'react-native';

export default class Counter extends Component {
    state = {
        number: this.props.startAt
    };

    addOne() {
        this.setState({ number: this.state.number + 1});
    }

    clear = () => {
        this.setState({ number: this.props.startAt});
    }

    render() {
        return (
            <View>
                <Text style={{fontSize: 40}}>{this.state.number}</Text>
                <TouchableHighlight
                    onPress={() => this.addOne()}
                    onLongPress={this.clear}>
                    <Text>Add/Reset</Text>
                </TouchableHighlight>
            </View>
        );
    }
}