import React from 'react';
import { View, Text } from 'react-native';
import Standard from '../style/Standard';

function oddOrEven(n) {
    const v = n % 2 == 0 ? 'Par' : 'Impar';
    return <Text style={Standard.ex}>{v}</Text>
}

export default props =>
    <View>
        {oddOrEven(props.number)}
    </View>

