/*jshint esversion: 6 */
import React from 'react';
import { View, StyleSheet } from 'react-native';
import Simple from './components/Simple';
import OddOrEven from './components/OddOrEven';
import Invert, { Lottery } from './components/Multi';

export default function() {
  return (
    <View style={styles.container}>
      <Simple message="True Flexible!" />
      <OddOrEven number={22} />
      <Invert text='React Native' />
      <Lottery numbers={5} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})