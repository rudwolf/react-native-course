/**
 * @format
 */

import {AppRegistry} from 'react-native';
// import App from './src/App';
import {name as appName} from './app.json.js';
import Menu from './src/Menu';

AppRegistry.registerComponent(appName, () => Menu);
